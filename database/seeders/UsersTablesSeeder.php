<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;

class UsersTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'name' => 'john smith',
                'email' => 'john_smith@gmail.com',
                'password' => Hash::make('10203040'),
                'created_at'=>Carbon::now()

            ],
            [
                'name' => 'mostafa hekmati',
                'email' => 'mostafahekmatiii22@gmail.com',
                'password' => Hash::make('808080'),
                'created_at'=>Carbon::now()
            ]
        ];
        User::query()->insert($users);
    }
}
