<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Http\Request;
use Validator;
use Auth;

class MainController extends Controller
{
    public function create()
    {
        return view('register');
    }
    public function store(Request $request)
    {

        $user=new User();
        $user-> name = $request->input('name');
        $user-> email = $request->input('email');
        $user-> password = $request->input('password');
        $user->save();



        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required'
        ]);

        $user = User::create(request(['name', 'email', 'password']));
        auth()->login($user);
        return redirect()->to('/');
    }

    ///////////////////////////
    public function index(){
        return view('login');
    }
    public function checklogin(Request $request){

        $this->validate($request, [
            'email'   => 'required|email',
            'password'  => 'required|alphaNum|min:3'
        ]);

                $user_data = array(
                    'email'  => $request->get('email'),
                    'password' => $request->get('password')
                );

                    if(Auth::attempt($user_data))
                    {
                        return redirect('main/successlogin');
                    }
                        else
                        {
                            return back()->with('error', 'Wrong Login Details');
                        }

    }
    function successlogin()
    {
        return view('successlogin');
    }

    function logout()
    {
        Auth::logout();
        return redirect('main');
    }

}
